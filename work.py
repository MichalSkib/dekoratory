# DEKORATORY
import time


def printer(func):

    def inner():
        start_time = time.time()
        func()
        end_time = time.time()
        time_delta = end_time - start_time
        message = f'Czas wykonania funkcji: {time_delta} s'
        print(message)
        return func
    return inner


@printer
def show():
    time.sleep(5)
    print("FUNKCJA")


def map_to_int(func):

    def inner():
        value = func()
        try:
            value = int(value)
        except ValueError:
            value = value
        return value
    return inner


def map_to(mode):

    def inner(func):

        def deeper():
            value = func()
            if mode == 'int':
                try:
                    return int(value)
                except ValueError:
                    pass
            elif mode == 'str':
                try:
                    return str(value)
                except ValueError:
                    pass
            elif mode == 'float':
                try:
                    return int(value)
                except ValueError:
                    pass
            else:
                return value
            return value
        return deeper
    return inner


@map_to(mode="float")
def get_age():
    age = input("Podaj wiek: ")
    return age


# age = get_age()
# print(age, type(age))

def duplicate(value):
    return value * 2


def assign(mode):

    def inner(func):

        def deeper():
            value = func()
            if mode == "duplicate":
                return duplicate(value)
        return deeper
    return inner


@assign(mode="duplicate")
def get_number():
    return 2
